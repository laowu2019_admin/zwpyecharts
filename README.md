# 百度可视化库

#### 介绍
ECharts 是一个由百度开源的数据可视化工具，凭借着良好的交互性，精巧的图表设计，得到了众多开发者的认可。而 Python 是一门富有表达力的语言，很适合用于数据处理。当数据分析遇上数据可视化时，pyecharts 诞生了。百度可视化 是 pyecharts 的中文版。

#### 安装教程

1.  更新草蟒标准库：python -m pip install --upgrade grasspy-stdlib
2.  安装百度可视化库：python -m pip install --upgrade grasspy-pyecharts
3.  若要运行示例，还须安装网页IO库：python -m pip install --upgrade grasspy-pywebio

#### 使用说明

1.  示例位于本代码库的 tests 目录中
2.  [pyecharts 文档](https://pyecharts.org/#/) 和 [pyecharts 图库](https://gallery.pyecharts.org/#/README)
3.  [echarts 文档和图库](https://echarts.apache.org/zh/index.html)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
