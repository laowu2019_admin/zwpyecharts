导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

导入 日时
导入 随机数

从 百度可视化 导入 选项
从 百度可视化.图表 导入 〇日历图
从 百度可视化.全局变量 导入 匚朝向

套路 主函数():
    开始 = 日时.〇日期(2023, 1, 1)
    结束 = 日时.〇日期(2023, 12, 31)
    数据 = [
        [字符串型(开始 + 日时.〇时间差(天数=i)), 随机数.随机整数(1000, 25000)]
        取 i 于 范围((结束 - 开始).天数 + 1)
    ]

    c = (
        〇日历图()
        .添加("", 数据, 日历选项々=选项.〇日历选项々(范围_="2023"))
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(主标题="日历 - 2023 年微信步数情况"),
            视觉映射选项々=选项.〇视觉映射选项々(
                最大值_=20000,
                最小值_=500,
                朝向=匚朝向.水平,
                是分段型=True,
                位置_上="230px",
                位置_左="100px",
            ),
        )
    )

    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)
