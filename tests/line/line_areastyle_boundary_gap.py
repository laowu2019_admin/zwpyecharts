导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 百度可视化.图表 导入 〇折线图
从 百度可视化.捏造器 导入 捏造器
从 百度可视化 导入 选项

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

套路 主函数():
    c = (
        〇折线图()
        .添加x轴(捏造器.机选())
        .添加y轴("商家A", 捏造器.值々(), 平滑=真)
        .添加y轴("商家B", 捏造器.值々(), 平滑=真)
        .设置系列选项々(
            区域填充样式选项々=选项.〇区域填充样式选项々(不透明度=0.5),
            标签选项々=选项.〇标签选项々(显示=False),
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(主标题='折线图 - 面积图 (紧贴Y轴)'),
            x轴选项々=选项.〇坐标轴选项々(
                坐标轴刻度选项々=选项.〇坐标轴刻度选项々(同标签对齐=假),
                比例=真,  # 这个参数到底有何用？
                两边留白=假,
            ),
        )
    )
    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)