导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 百度可视化.图表 导入 〇折线图
从 百度可视化.全局变量 导入 匚记号类型, 匚坐标轴类型, 匚标记类型
从 百度可视化 导入 选项

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

套路 主函数():
    星期列表 = ["周一", "周二", "周三", "周四", "周五", "周六", "周日"]
    最高温度 = [11, 11, 15, 13, 12, 13, 10]
    最低温度 = [1, -2, 2, 5, 3, 2, 0]
    # 最低温度 = [1, -2, 2, 5, 空, 2, 0]

    c = (
        〇折线图(选项.〇初始化选项々(宽度="1600px", 高度="800px"))
        .添加x轴(x轴数据=星期列表)
        .添加y轴(
            系列名称="最高气温",
            y轴=最高温度,
            标记点选项々=选项.〇标记点选项々(
                数据=[
                    选项.〇标记点数据项(类型_=匚标记类型.最大值, 名称="最大值"),
                    选项.〇标记点数据项(类型_=匚标记类型.最小值, 名称="最小值"),
                ]
            ),
            标记线选项々=选项.〇标记线选项々(
                数据=[选项.〇标记线数据项(类型_=匚标记类型.平均值, 名称="平均值")]
            ),
        )
        .添加y轴(
            系列名称="最低气温",
            y轴=最低温度,
            # 连接空数据=真,
            标记点选项々=选项.〇标记点选项々(
                数据=[选项.〇标记点数据项(值=-2, 名称="周最低", x=1, y=-1.5)]  # 这个显示在哪里？
            ),
            标记线选项々=选项.〇标记线选项々(
                数据=[
                    选项.〇标记线数据项(类型_=匚标记类型.平均值, 名称="平均值"),
                    选项.〇标记线数据项(记号="circle", x="90%", y="max"), # 这个显示在哪里？
                    选项.〇标记线数据项(记号=匚记号类型.矩形, 类型_=匚标记类型.最大值, 名称="最高点"),
                ]
            ),
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(主标题="未来一周气温变化", 副标题="纯属虚构"),
            提示框选项々=选项.〇提示框选项々(触发类型="坐标轴"),
            工具箱选项々=选项.〇工具箱选项々(显示=True),
            x轴选项々=选项.〇坐标轴选项々(类型_=匚坐标轴类型.类目, 两边留白=False,),
        )
    )
    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)