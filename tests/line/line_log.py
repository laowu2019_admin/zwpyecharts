导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 百度可视化.图表 导入 〇折线图
从 百度可视化.全局变量 导入 匚坐标轴类型
从 百度可视化 导入 选项

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

套路 主函数():
    # x_data = ["一", "二", "三", "四", "五", "六", "七", "八", "九"]
    x_data = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    y_data_3 = [1, 3, 9, 27, 81, 247, 741, 2223, 6669]
    y_data_2 = [1, 2, 4, 8, 16, 32, 64, 128, 256]
    y_data_05 = [1 / 2, 1 / 4, 1 / 8, 1 / 16, 1 / 32, 1 / 64, 1 / 128, 1 / 256, 1 / 512]

    c = (
        〇折线图(选项.〇初始化选项々(宽度="1600px", 高度="800px"))
        .添加x轴(x轴数据=x_data)
        .添加y轴(
            系列名称="1/2的指数", y轴=y_data_05, 线条样式选项々=选项.〇线条样式选项々(宽度=2)
        )
        .添加y轴(
            系列名称="2的指数", y轴=y_data_2, 线条样式选项々=选项.〇线条样式选项々(宽度=2)
        )
        .添加y轴(
            系列名称="3的指数", y轴=y_data_3, 线条样式选项々=选项.〇线条样式选项々(宽度=2)
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(主标题="对数轴示例", 位置_左="center"),
            提示框选项々=选项.〇提示框选项々(触发类型="数据项", 格式器="{a} <br/>{b} : {c}"),
            图例选项々=选项.〇图例选项々(位置_左="left"),
            x轴选项々=选项.〇坐标轴选项々(类型_=匚坐标轴类型.类目, 名称="x"),
            y轴选项々=选项.〇坐标轴选项々(
                类型_=匚坐标轴类型.对数,
                名称="y",
                分割线选项々=选项.〇分割线选项々(显示=True),
                比例=True,  # ！！！比例的意义
            ),
        )
    )
    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)