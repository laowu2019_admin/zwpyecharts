导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 百度可视化.图表 导入 〇饼图
从 百度可视化 导入 选项
从 百度可视化.全局变量 导入 匚朝向

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

inner_x_data = ["直达", "营销广告", "搜索引擎"]
inner_y_data = [335, 679, 1548]
内数据对 = [list(z) for z in zip(inner_x_data, inner_y_data)]

outer_x_data = ["直达", "邮件营销", "联盟广告", "视频广告", "百度", "谷歌", "必应", "其他"]
outer_y_data = [335, 310, 234, 135, 1048, 251, 147, 102]
外数据对 = [list(z) for z in zip(outer_x_data, outer_y_data)]

套路 主函数():
    c = (
        〇饼图(选项.〇初始化选项々(宽度="1600px", 高度="800px"))
        .添加(
            系列名称='访问来源',
            数据对=内数据对,
            半径=[0, "30%"],
            标签选项々=选项.〇标签选项々(位置='inner')
        )
        .添加(
            系列名称='访问来源',
            数据对=外数据对,
            半径=["40%", "55%"],
            标签选项々=选项.〇标签选项々(
                位置="outside",
                格式器="{a|{a}}{abg|}\n{hr|}\n {b|{b}: }{c}  {per|{d}%}  ",
                背景颜色="#eee",
                边框颜色="#aaa",
                边框宽度=1,
                边框半径=4,
                富文本={
                    "a": {"color": "#999", "lineHeight": 22, "align": "center"},
                    "abg": {
                        "backgroundColor": "#e3e3e3",
                        "width": "100%",
                        "align": "right",
                        "height": 22,
                        "borderRadius": [4, 4, 0, 0],
                    },
                    "hr": {
                        "borderColor": "#aaa",
                        "width": "100%",
                        "borderWidth": 0.5,
                        "height": 0,
                    },
                    "b": {"fontSize": 16, "lineHeight": 33},
                    "per": {
                        "color": "#eee",
                        "backgroundColor": "#334455",
                        "padding": [2, 4],
                        "borderRadius": 2,
                    },
                },
            ),
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(
                主标题="饼图 - 大圈套小圈",
                位置_左='中',
            ),
            图例选项々=选项.〇图例选项々(位置_左='左', 朝向=匚朝向.竖直)
        )
        .设置系列选项々(
            提示框选项々=选项.〇提示框选项々(触发类型="数据项", 格式器="{a} <br/>{b}: {c} ({d}%)"),
        )
    )
    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)
