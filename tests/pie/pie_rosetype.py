导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 百度可视化.图表 导入 〇饼图
从 百度可视化 导入 选项
从 百度可视化.全局变量 导入 匚朝向
从 百度可视化.捏造器 导入 捏造器

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

套路 主函数():
    v = 捏造器.机选()
    c = (
        〇饼图()
        .添加(
            "",
            [list(z) for z in zip(v, 捏造器.值々())],
            半径=["30%", "75%"],
            中心=["25%", "50%"],
            玫瑰类型='radius',
            标签选项々=选项.〇标签选项々(位置='inner')
        )
        .添加(
            "",
            [list(z) for z in zip(v, 捏造器.值々())],
            半径=["30%", "75%"],
            中心=["75%", "50%"],
            玫瑰类型='area',
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(
                主标题="饼图 - 玫瑰图示例",
            ),
            # 图例选项々=选项.〇图例选项々(位置_左='左', 朝向=匚朝向.竖直)
        )
        .设置系列选项々(
            提示框选项々=选项.〇提示框选项々(触发类型="数据项", 格式器="{a} <br/>{b}: {c} ({d}%)"),
        )
    )
    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)
