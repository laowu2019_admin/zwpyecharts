导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 百度可视化.图表 导入 〇饼图
从 百度可视化 导入 选项
从 百度可视化.全局变量 导入 匚朝向

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

x数据 = ["直接访问", "邮件营销", "联盟广告", "视频广告", "搜索引擎"]
y数据 = [335, 310, 274, 235, 400]
数据对 = [列表型(z) 取 z 于 组队(x数据, y数据)]
数据对.排序(键=雷锋 x: x[1])

套路 主函数():
    c = (
        〇饼图(选项.〇初始化选项々(宽度="1600px", 高度="800px"))
        .添加(
            系列名称='访问来源',
            数据对=数据对,
            半径=["50%", "70%"],
            标签选项々=选项.〇标签选项々(
                位置="outside",
                格式器="{a|{a}}{abg|}\n{hr|}\n {b|{b}: }{c}  {per|{d}%}  ",
                背景颜色="#eee",
                边框颜色="#aaa",
                边框宽度=1,
                边框半径=4,
                富文本={
                    "a": {"color": "#999", "lineHeight": 22, "align": "center"},
                    "abg": {
                        "backgroundColor": "#e3e3e3",
                        "width": "100%",
                        "align": "right",
                        "height": 22,
                        "borderRadius": [4, 4, 0, 0],
                    },
                    "hr": {
                        "borderColor": "#aaa",
                        "width": "100%",
                        "borderWidth": 0.5,
                        "height": 0,
                    },
                    "b": {"fontSize": 16, "lineHeight": 33},
                    "per": {
                        "color": "#eee",
                        "backgroundColor": "#334455",
                        "padding": [2, 4],
                        "borderRadius": 2,
                    },
                },
            ),
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(
                主标题="饼图 - 甜甜圈",
            ),
        )
    )
    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)
