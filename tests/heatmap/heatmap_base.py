导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

从 百度可视化 导入 选项
从 百度可视化.图表 导入 〇热力图
从 百度可视化.捏造器 导入 捏造器
导入 随机数

套路 主函数():

    值 = [[i, j, 随机数.随机整数(0, 50)] 取 i 于 范围(24) 取 j 于 范围(7)]
    c = (
        〇热力图()
        .添加x轴(捏造器.二十四小时)
        .添加y轴(
            "系列0",
            捏造器.一周,
            值,
            标签选项々=选项.〇标签选项々(显示=True, 位置="内部"),
        )
        .设置全局选项々(
            标题选项々=选项.〇标题选项々(主标题="热力图 - 标签显示"),
            视觉映射选项々=选项.〇视觉映射选项々(),
        )
    )

    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)
