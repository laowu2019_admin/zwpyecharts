导入 sys
导入 os

sys.path.追加(os.路径.绝对路径(r'G:\grasspy_zwmodules\pyecharts\zwpyecharts\src'))

从 网页io 导入 启动服务器
从 网页io.输出 导入 *

从 百度可视化 导入 选项
从 百度可视化.图表 导入 〇关系图

套路 主函数():

    节点々 = [
        {"name": "结点1", "symbolSize": 10},
        {"name": "结点2", "symbolSize": 20},
        {"name": "结点3", "symbolSize": 30},
        {"name": "结点4", "symbolSize": 40},
        {"name": "结点5", "symbolSize": 50},
        {"name": "结点6", "symbolSize": 40},
        {"name": "结点7", "symbolSize": 30},
        {"name": "结点8", "symbolSize": 20},
    ]

    链接々 = []
    取 i 于 节点々:
        取 j 于 节点々:
            链接々.追加({"source": i.get("name"), "target": j.get("name")})
    
    c = (
        〇关系图()
        .添加("", 节点々, 链接々, 斥力因子=8000)
        .设置全局选项々(标题选项々=选项.〇标题选项々(主标题="关系图 - 基本示例"))
    )

    c.宽度 = '100%'
    输出html(c.渲染到笔记本())

如果 __名称__ == "__主体__":
    启动服务器(主函数, 调试=真, 端口=8080)
